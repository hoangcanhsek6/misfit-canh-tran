//
//  ListDevicesViewController.swift
//  Demo Core bluetooth
//
//  Created by Canh Tran on 10/17/16.
//  Copyright © 2016 MisFit. All rights reserved.
//

import UIKit
import CoreBluetooth

class ListDevicesViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var listDevicesTableView: UITableView!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupTableView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ListDevicesViewController.connectionChanged), name: BLServiceChangedStatusNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ListDevicesViewController.listDevicesChanged), name: BLServiceDidSetListDevices, object: nil)
        
        // Start discovery instance
        discoverySharedInstance.startScanning()
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Setup view
    
    private func setupTableView() {
        // Setup automatic row height
        listDevicesTableView.rowHeight = 55//UITableViewAutomaticDimension
    }
    
    
    @IBAction func refreshScanDevices(_ sender: AnyObject) {
        discoverySharedInstance.peripherals.removeAll()
        listDevicesTableView.reloadData()
        discoverySharedInstance.startScanning()
    }
    
    @IBAction func sortListDeviceByRSSI(_ sender: AnyObject) {
        discoverySharedInstance.peripherals.sort{ Int($0.deviceRSSI!)! > Int($1.deviceRSSI!)!}
        listDevicesTableView.reloadData()
    }
    
    // Notification observer actions
    func listDevicesChanged(notification: NSNotification) {
        listDevicesTableView.reloadData()
    }
    
    func connectionChanged(notification: NSNotification) {
        // Connection status changed. Indicate on GUI.
        let userInfo = notification.userInfo as! [String: Bool]
        DispatchQueue.main.async {
            // Set image based on connection status
            if let isConnected: Bool = userInfo["isConnected"] {
                if isConnected {
                    self.writeAnimation()
                } else {
                    
                }
            }
        }
    }
    
    
    func writeAnimation() {
        if let service = discoverySharedInstance.service {
            service.writeAnimation(animation: UInt8(animationCharacteristic))
        }
    }
    
    
}


// MARK:- Table view delegate & datasource
extension ListDevicesViewController : UITableViewDelegate, UITableViewDataSource {
    // DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoverySharedInstance.peripherals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceTableViewCell", for: indexPath) as! DeviceTableViewCell
        cell.deviceModel = discoverySharedInstance.peripherals[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if discoverySharedInstance.peripherals[indexPath.row].deviceSerial == mySerialDevice {
            self.performSegue(withIdentifier: "showDeviceDetail", sender: self)
        }
    }
}





