//
//  Service.swift
//  Demo Core bluetooth
//
//  Created by Canh Tran on 10/18/16.
//  Copyright © 2016 MisFit. All rights reserved.
//

import UIKit
import CoreBluetooth

let mySerialDevice                        = "W0C001202A"
let animationCharUUID                   = CBUUID(string: "acdbefaf-d19b-4d3f-ab68-995881f981fe")
let serviceUUID                         = CBUUID(string: "acdbefaf-d19b-4d3f-ab68-995881f981fe")
let BLServiceChangedStatusNotification  = Notification.Name("BLServiceChangedStatusNotification")

let animationCharacteristic             = 0x02f105

class Service: NSObject, CBPeripheralDelegate {
    var peripheral: CBPeripheral?
    var positionCharacteristic: CBCharacteristic?
    
    init(initWithPeripheral peripheral: CBPeripheral) {
        super.init()
        
        self.peripheral = peripheral
        self.peripheral?.delegate = self
    }
    
    deinit {
        self.reset()
    }
    
    func startDiscoveringServices() {
        self.peripheral?.discoverServices(nil)
    }
    
    func reset() {
        if peripheral != nil {
            peripheral = nil
        }
        
        // Deallocating therefore send notification
        self.sendBLServiceNotificationWithIsBluetoothConnected(isBluetoothConnected: false)
    }
    
    // Mark: - CBPeripheralDelegate
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        let uuidsForBTService: [CBUUID] = [animationCharUUID]
        
        if (peripheral != self.peripheral) {
            // Wrong Peripheral
            return
        }
        
        if (error != nil) {
            return
        }
        
        if ((peripheral.services == nil) || (peripheral.services!.count == 0)) {
            // No Services
            return
        }
        
        for service in peripheral.services! {
            if service.uuid == serviceUUID {
                peripheral.discoverCharacteristics(uuidsForBTService, for: service)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if (peripheral != self.peripheral) {
            // Wrong Peripheral
            return
        }
        
        if (error != nil) {
            return
        }
        
        if let characteristics = service.characteristics {
            for characteristic in characteristics {
                if characteristic.uuid == animationCharUUID {
                    self.positionCharacteristic = (characteristic)
                    peripheral.setNotifyValue(true, for: characteristic)
                    // Send notification that Bluetooth is connected and all required characteristics are discovered
                    self.sendBLServiceNotificationWithIsBluetoothConnected(isBluetoothConnected: true)
                }
            }
        }
    }
    
    // MARK: - Private
    
    func writeAnimation(animation: UInt8) {
        // See if characteristic has been discovered before writing to it
        if let positionCharacteristic = self.positionCharacteristic {
            // Need a mutable var to pass to writeValue function
            var animationValue = animation
            let data = NSData(bytes: &animationValue, length: MemoryLayout<UInt8>.size)
            self.peripheral?.writeValue(data as Data, for: positionCharacteristic, type: CBCharacteristicWriteType.withResponse)
        }
    }
    
    func sendBLServiceNotificationWithIsBluetoothConnected(isBluetoothConnected: Bool) {
        let connectionDetails = ["isConnected": isBluetoothConnected]
        NotificationCenter.default.post(name: BLServiceChangedStatusNotification, object: self, userInfo: connectionDetails)
    }
}
