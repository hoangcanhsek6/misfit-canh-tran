//
//  Device.swift
//  Demo Core bluetooth
//
//  Created by Canh Tran on 10/17/16.
//  Copyright © 2016 MisFit. All rights reserved.
//

import UIKit
import CoreBluetooth

class Device: NSObject {
    var deviceName : String? = ""
    var deviceRSSI : String? = ""
    var deviceSerial : String? = ""
    var peripheral : CBPeripheral?
    
    override init() {
        super.init()
    }
    
    func createObjectWith(deviceName: String?, deviceRSSI: String?, deviceSerial: String?, peripheral: CBPeripheral) {
        self.deviceName = deviceName
        self.deviceRSSI = deviceRSSI
        self.deviceSerial = deviceSerial
        self.peripheral = peripheral
    }
    
}
