//
//  StringExtension.swift
//  Demo Core bluetooth
//
//  Created by Canh Tran on 10/17/16.
//  Copyright © 2016 MisFit. All rights reserved.
//

import UIKit

extension String {
    static func className(aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
}
