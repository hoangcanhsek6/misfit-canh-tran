//
//  DeviceTableViewCell.swift
//  Demo Core bluetooth
//
//  Created by Canh Tran on 10/17/16.
//  Copyright © 2016 MisFit. All rights reserved.
//

import UIKit

class DeviceTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceRSSILabel: UILabel!
    
    var deviceModel : Device? {
        didSet {
            self.setupCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    private func setupCell() {
        deviceNameLabel.text = " \(deviceModel!.deviceName!) - '\(deviceModel!.deviceSerial!)' "
        deviceRSSILabel.text = deviceModel?.deviceRSSI
        
    }
    
    
}



