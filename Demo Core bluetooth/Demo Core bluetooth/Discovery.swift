//
//  Discovery.swift
//  Demo Core bluetooth
//
//  Created by Canh Tran on 10/18/16.
//  Copyright © 2016 MisFit. All rights reserved.
//

import UIKit
import CoreBluetooth

let discoverySharedInstance             = Discovery()
let BLServiceDidSetListDevices          = Notification.Name("BLServiceDidSetListDevices")

class Discovery: NSObject, CBCentralManagerDelegate {
    
    private var centralManager: CBCentralManager?
    private var peripheral: CBPeripheral?
    
    var peripherals = [Device]() {
        didSet {
            NotificationCenter.default.post(name: BLServiceDidSetListDevices, object: nil, userInfo: nil)
        }
    }
    
    override init() {
        super.init()
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    var service: Service? {
        didSet {
            if let service = self.service {
                service.startDiscoveringServices()
            }
        }
    }
    
    func startScanning() {
        if let central = centralManager {
            central.scanForPeripherals(withServices: nil, options: nil)
        }
    }
    
    // MARK: - CBCentralManagerDelegate
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let deviceName = (advertisementData[CBAdvertisementDataLocalNameKey] as? String)
        let deviceSerial =  getSerialNumber(advertisementData: advertisementData)
        guard deviceName != nil && deviceSerial != nil else { return }
        let device = Device()
        device.createObjectWith(deviceName: deviceName, deviceRSSI: RSSI.description, deviceSerial: deviceSerial, peripheral: peripheral)
        
        // Connect with my device
        if deviceSerial?.contains(mySerialDevice) == true {
            if self.peripheral?.state == CBPeripheralState.disconnected {
                print("Disconnected")
            }
            self.centralManager?.stopScan()
            self.peripheral = peripheral
            self.centralManager?.connect(peripheral, options: nil)
        }
        peripherals.append(device)
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        // Create new service class
        if (peripheral == self.peripheral) {
            self.service = Service(initWithPeripheral: peripheral)
        }
        
        // Stop scanning for new devices
        central.stopScan()
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        // See if it was our peripheral that disconnected
        if (peripheral == self.peripheral) {
            self.service = nil
            self.peripheral = nil
        }
        
        // Start scanning for new devices
        self.startScanning()
    }
    
    // MARK: - Private
    
    func clearDevices() {
        self.service = nil
        self.peripheral = nil
    }
    

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("State is : \(central.state.rawValue)")
        switch central.state {
        case .poweredOff:
            print("Power OFF")
            self.clearDevices()
            break
        case .poweredOn:
            self.startScanning()
            break
        case .resetting:
            self.clearDevices()
            break
        default:
            break
        }
    }

    
    // MARK: - Helper
    // Get serial string with advertiement data
    func getSerialNumber(advertisementData: [String : Any]) -> String? {
        if let value = advertisementData[CBAdvertisementDataManufacturerDataKey] {
            let data = value as! NSData
            if data.length < 12 {
                return nil
            }
            var serialNumber = NSString(bytes: data.bytes, length: data.length, encoding: String.Encoding.ascii.rawValue)
            serialNumber = serialNumber?.substring(from: 2) as NSString?
            serialNumber = serialNumber?.substring(to: 10) as NSString?
            return serialNumber as? String
        }
        return "Not found"
    }
    
}
